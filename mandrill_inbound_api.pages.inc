<?php
/**
 * @file
 * Page callbacks for the Mandrill Inbound API module.
 */

/**
 * Page callback: Handle inbound requests.
 *
 * @param array $pattern_prefix
 *   The pattern prefix used in the email pattern and route webhook url.
 */
function mandrill_inbound_api_handle_inbound($pattern_prefix) {
  $route_info = mandrill_inbound_api_get_route($pattern_prefix);

  // If Mandrill is pinging the url to verify it exists, we do not need to
  // process anything.
  if (variable_get('mandrill_inbound_api_add_route_processing', FALSE)) {
    watchdog('mandrill_inbound_api', 'Add route processing enabled. Assumption is mandrill is pinging the webhook url !url', array(
      '!url' => $route_info['url'],
    ));
    // Immediately reset this to FALSE to avoid the possibility of it being left
    // open and leaving this endpoint exposed.
    variable_set('mandrill_inbound_api_add_route_processing', FALSE);
    return t('Mandrill Inbound API: Add Route Processing');
  }

  // The access callback should check for missing mandrill_events, however we
  // make the additional check here to handle testing mode enabled.
  if (empty($_POST['mandrill_events']) || !$mandrill_events = json_decode($_POST['mandrill_events'])) {
    watchdog('mandrill_inbound_api', 'Missing mandrill events for !url.', array('!url' => $route_info['url']));
    return t('Mandrill Inbound Processing: Missing Mandrill Events');
  }

  // Get the testing mode action.
  // If testing mode enabled and action is not the default action (pass through
  // to module) then either log or forward the message.
  $testing_mode_action = variable_get('mandrill_inbound_api_testing_mode_action', 'log');
  if (variable_get('mandrill_inbound_api_testing_mode_enabled', FALSE) && $testing_mode_action != 'default') {

    if ($testing_mode_action == 'log') {
      $debug = array(
        'route_info' => $route_info,
        'mandrill_events' => $mandrill_events,
      );
      $debug_log = '<pre>' . print_r($debug, TRUE) . '</pre>';
      watchdog('mandrill_inbound_api_debug', $debug_log);
    }
    elseif ($testing_mode_action == 'email') {
      if ($testing_mode_email = variable_get('mandrill_inbound_api_testing_mode_email', '')) {
        foreach ($mandrill_events as $event) {
          $params['route_info'] = $route_info;
          $params['msg'] = mandrill_inbound_api_parse_message($event->msg);
          drupal_mail('mandrill_inbound_api', 'test_mode_email', $testing_mode_email, language_default(), $params);
        }
      }
    }
  }
  // Testing mode disabled or testing mode action is the default (pass to
  // module) action.
  else {
    if (function_exists($route_info['callback'])) {
      foreach ($mandrill_events as $delta => $event) {
        $msg = mandrill_inbound_api_parse_message($event->msg);
        $email_identifier = mandrill_inbound_api_get_email_identifier($msg->email);
        $route_info['callback']($msg, $email_identifier);
      }
    }
  }
}
