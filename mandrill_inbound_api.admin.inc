<?php
/**
 * @file
 * Administrative forms / functions for the Mandrill Inbound API.
 */

/**
 * Admin page callback: Configuration Page.
 */
function mandrill_inbound_api_configure() {
  $output['testing_mode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Testing Mode'),
    '#description' => t('By default any route that is not assigned to a domain simply ignores any inbound emails. Enabling testing mode bypasses that behavior with one of your choosing.'),
    '#collapsible' => TRUE,
    '#collapsed' => !variable_get('mandrill_inbound_api_testing_mode_enabled', FALSE),
    'form' => drupal_get_form('mandrill_inbound_api_testing_mode_form'),
  );
  $output['domain_configure'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain Configuration'),
    'form' => drupal_get_form('mandrill_inbound_api_domains_configure_form'),
  );
  $output['routes_configure'] = array(
    '#type' => 'fieldset',
    '#title' => t('Route Configuration'),
    '#description' => t('Note: Mandrill must have access to the route webhook URL to assign a domain.'),
    'form' => drupal_get_form('mandrill_inbound_api_routes_configure_form'),
  );
  return $output;
}

/**
 * Domain Configuration Form.
 */
function mandrill_inbound_api_domains_configure_form($form, &$form_state) {
  global $base_url;
  $inbound_domains = mandrill_inbound_api_get_domains(TRUE);

  // Add domain actions.
  foreach ($inbound_domains as $domain => $inbound_domain) {
    $inbound_domains[$domain]['actions'] = mandrill_inbound_api_get_domain_actions($domain);
  }

  $base_url_parts = parse_url($base_url);
  $example_domain = "example.{$base_url_parts['host']}";

  $form['add_domain'] = array(
    '#type' => 'container',
  );
  $form['add_domain']['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#title_display' => 'invisible',
    '#attributes' => array(
      'placeholder' => $example_domain,
    ),
    '#required' => TRUE,
  );
  $form['add_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Domain')
  );

  $form['inbound_domains'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Domain'),
      t('Created'),
      t('Valid MX'),
      '',
    ),
    '#rows' => $inbound_domains,
    '#empty' => t('There are no inbound domains configured at this time.'),
  );

  return $form;
}

/**
 * Validate handler: Add domain.
 */
function mandrill_inbound_api_domains_configure_form_validate($form, &$form_state) {
  $url = 'http://' . $form_state['values']['domain'];

  if (!valid_url($url, TRUE)) {
    form_set_error('domain', t('Please provide a valid domain.'));
  }
  else {
    $url_parts = parse_url($url);
    if ($url_parts['host'] != $form_state['values']['domain']) {
      form_set_error('domain', t('Please provide a valid url.'));
    }
  }
}

/**
 * Submit handler: Add domain.
 */
function mandrill_inbound_api_domains_configure_form_submit($form, &$form_state) {
  mandrill_add_inbound_domain($form_state['values']['domain']);
  drupal_set_message(t('@domain has been added.', array('@domain' => $form_state['values']['domain'])));
}

/**
 * Admin page callback: Delete domain.
 *
 * @param string $domain
 */
function mandrill_inbound_api_domain_delete_form($form, &$form_state, $domain) {
  $form['domain'] = array(
    '#type' => 'value',
    '#value' => $domain,
  );

  $domain_routes = mandrill_inbound_api_get_domain_routes($domain, TRUE);
  $routes = array();

  foreach ($domain_routes as $delta => $domain_route) {
    $routes[$delta] = array(
      'id' => $domain_route['id'],
      'pattern' => "{$domain_route['pattern']}@{$domain}",
      'url' => $domain_route['url'],
      'domain' => $domain_route['domain'],
    );
  }

  $form['routes'] = array(
    '#type' => 'value',
    '#value' => $routes,
  );

  $form['message_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('messages', 'warning'),
    ),
    'message' => array(
      '#markup' => t('The following routes will be disabled.'),
    ),
    '#access' => !empty($routes),
  );
  $form['domain_routes'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('ID'),
      t('Pattern'),
      t('Webhook'),
      t('Domain'),
    ),
    '#rows' => $routes,
    '#empty' => t('There are no routes configured for this domain.'),
  );

  return confirm_form(
    $form,
    t('Delete domain @domain?', array('@domain' => $domain)),
    MANDRILL_INBOUND_API_CONFIG_URL,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler: Delete domain.
 */
function mandrill_inbound_api_domain_delete_form_submit($form, &$form_state) {
  // Delete all of the associated routes and then delete the domain.
  foreach ($form_state['values']['routes'] as $route_info) {
    mandrill_inbound_api_delete_inbound_route($route_info['id'], $route_info['url']);
  }
  mandrill_delete_inbound_domain($form_state['values']['domain']);
  $form_state['redirect'] = MANDRILL_INBOUND_API_CONFIG_URL;
}

/**
 * Routes Configuration Form
 */
function mandrill_inbound_api_routes_configure_form($form, &$form_state) {
  $routes = mandrill_inbound_api_get_routes(TRUE);

  $form['routes'] = array(
    '#theme' => 'routes_configure_list',
    '#tree' => TRUE,
    '#header' => array(
      t('Email Pattern'),
      t('Webhook URL'),
      t('Domain'),
    ),
  );
  $options = mandrill_inbound_api_get_domain_options();

  foreach ($routes as $delta => $route_info) {
    $form['routes'][$delta] = array(
      '#mandrill_id' => !empty($route_info['id']) ? $route_info['id'] : NULL,
      'pattern' => array(
        '#markup' => $route_info['pattern'],
      ),
      'url' => array(
        '#markup' => $route_info['url'],
      ),
      'domain' => array(
        '#type' => 'select',
        '#title' => t('Domain'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => !empty($options[$route_info['domain']]) ? $route_info['domain'] : 'disabled',
        '#required' => TRUE,
      ),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Submit handler: Routes Configuration Form.
 *
 * @todo Mandrill module does not contain update/delete routes functions.
 */
function mandrill_inbound_api_routes_configure_form_submit($form, &$form_state) {
  $routes_cached = mandrill_inbound_api_get_routes();
  $routes_values = array_replace_recursive($routes_cached, $form_state['values']['routes']);

  foreach ($routes_values as $delta => $route_info) {
    if ($routes_values[$delta] == $routes_cached[$delta]) {
      continue;
    }

    // Remove the route to either leave as disabled or add it to another domain.
    // We can only remove a route from Mandrill that provides an 'id'.
    if (!empty($route_info['id'])) {
      mandrill_inbound_api_delete_inbound_route($route_info['id'], $route_info['url']);
    }

    // When adding a route, Mandrill will ping the URL. Since the access control
    // function will return an access denied and if the site contains a user
    // login redirect, then Mandrill will think that is the actual URL. So we
    // use a variable to temporarily open up access.
    if ($route_info['domain'] != 'disabled') {
      variable_set('mandrill_inbound_api_add_route_processing', TRUE);
      mandrill_add_inbound_route($route_info['domain'], $route_info['pattern'], $route_info['url']);
      variable_set('mandrill_inbound_api_add_route_processing', FALSE);
    }
  }
}

/**
 * Testing mode form.
 */
function mandrill_inbound_api_testing_mode_form($form, &$form_state) {
  $form['mandrill_inbound_api_testing_mode_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('mandrill_inbound_api_testing_mode_enabled', FALSE),
  );
  $form['mandrill_inbound_api_testing_mode_access_bypass'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Inbound Access Control'),
    '#description' => t('Bypass Drupal access control when Using a 3rd party service (e.g. <a href="https://ngrok.com/">ngrok</a>) as the webhook.'),
    '#default_value' => variable_get('mandrill_inbound_api_testing_mode_access_bypass', FALSE),
    '#states' => array(
      'visible' => array(
        ':input[name="mandrill_inbound_api_testing_mode_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['mandrill_inbound_api_testing_mode_action'] = array(
    '#type' => 'select',
    '#title' => t('Test Action'),
    '#description' => t('Choose what should happen when processing an inbound email.'),
    '#options' => array(
      'default' => t('Pass through to implementing module.'),
      'log' => t('Log the email'),
      'email' => t('Forward the email'),
    ),
    '#default_value' => variable_get('mandrill_inbound_api_testing_mode_action', 'default'),
    '#states' => array(
      'visible' => array(
        ':input[name="mandrill_inbound_api_testing_mode_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['mandrill_inbound_api_testing_mode_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('Forward inbound emails to this location. An empty value will default to !site_email', array('!site_email' => variable_get('site_mail', ''))),
    '#default_value' => variable_get('mandrill_inbound_api_testing_mode_email', variable_get('site_mail', '')),
    '#states' => array(
      'visible' => array(
        ':input[name="mandrill_inbound_api_testing_mode_enabled"]' => array('checked' => TRUE),
        ':input[name="mandrill_inbound_api_testing_mode_action"]' => array('value' => 'email'),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Get domains for form options.
 */
function mandrill_inbound_api_get_domain_options() {
  $domains = mandrill_inbound_api_get_domains();
  $domains = array_merge(array('disabled' => t('Disabled')), drupal_map_assoc(array_keys($domains)));
  return $domains;
}
