<?php
/**
 * @file
 * Theme functions for the Mandrill Inbound API module.
 */

/**
 * Theme function for mandrill_inbound_api_routes_configure form.
 */
function theme_routes_configure_list(&$variables) {
  global $base_url;
  $urlparts = parse_url($base_url);

  $content = array();
  $routes = $variables['routes'];

  $content['routes'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Email Pattern'),
      t('Webhook URL'),
      t('Domain'),
    ),
    '#rows' => array(),
  );

  foreach (element_children($routes) as $route_id) {
    $row = array();
    $domain = $routes[$route_id]['domain']['#default_value'];
    foreach (element_children($routes[$route_id]) as $element_id) {
      // Make the pattern look like the full email address.
      if ($element_id == 'pattern' && $domain != 'disabled') {
        $routes[$route_id][$element_id]['#markup'] .= "@{$domain}";
      }
      $row[] = drupal_render($routes[$route_id][$element_id]);
    }
    $content['routes']['#rows'][] = $row;
  }

  return drupal_render($content);
}
