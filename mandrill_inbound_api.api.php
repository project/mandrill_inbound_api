<?php
/**
 * @file
 * Mandrill Inbound API API Documentation.
 *
 * @todo Update the api documentation to Drupal API standards.
 */

/**
 * Implement Mandrill Route.
 *
 * This hook is used to tell Mandrill Inbound API about the email pattern to
 * look for and what callback to pass the email message to for processing.
 *
 * The email pattern takes on the form of [pattern_prefix]-* (e.g. test-*). All
 * emails received to an email address (e.g. test-123@mysite.com) that meets
 * that pattern will be passed off to the the callback. The pattern prefix may
 * only contain letters, numbers, and underscores.
 *
 * Mandrill Inbound API will handle receiving the inbound Mandrill events,
 * parsing out the email messages, and stripping out the identifier(s) from the
 * from email.  The email message object and identifiers (e.g. 123 from above
 * example) will be passed to the callback function.
 *
 * This hook should return an array of pattern_prefix => callback. Multiple
 * pattern_prefix => callback combinations can be returned.
 * e.g. array('[pattern_prefix'] => '[the_callback_function_name]')
 */
function hook_mandrill_inbound_api_routes_info() {
  return array(
    // Emails that follow the pattern (test-*) will be handed off to the
    // callback function mandrill_inbound_api_route_test.  You can see this in
    // action in the mandrill_inbound_api.module file.
    'test' => 'mandrill_inbound_api_route_test',
    // Emails that follow the pattern pm_reply-* where (*) is the thread ID will
    // be passed off to the Mandrill Inbound API Privatemsg module. You can see
    // this in action in the mandrill_inbound_api_privatemsg.module.
    'pm_reply' => 'mandrill_inbound_api_privatemsg_handle_inbound'
  );
}

/**
 * Alter Mandrill Route Info.
 *
 * Use this hook to alter the route information provided by other modules.
 * Routes are identified by [module_name]/[key].
 *
 * This could be useful by custom modules to automatically assign a route to an
 * existing domain or alter the callback used.
 */
function hook_mandrill_inbound_api_routes_info_alter(&$routes) {
  // Alter the test callback function.
  $delta = 'mandrill_inbound_api/test';
  if (!empty($routes[$delta])) {
    $routes[$delta]['callback'] = 'mymodule_new_test_callback';
  }
}
