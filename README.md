Mandrill Inbound API
====================

Introduction
------------

This module provides an API to interact with the [Mandrill Inbound Email
Processing](https://mandrill.zendesk.com/hc/en-us/categories/200277247-Inbound-Email-Processing) system. Modules are then able to use this API to setup email
patterns to receive emails and callbacks to process them.

If a user receives an email notification (e.g. new node, comment, private
message, etc.), then the user may respond via email to post a new comment
or private message. It is up to the module utilizing this API to ensure
that the notification emails being sent have a proper "reply to" email
pattern. This can typically be done by the module generating its own
notification emails or using [hook_mail_alter](https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_mail_alter/7.x).

This module does nothing on its own. There is no need to install this module
unless another module requires it.

**Features Include:**
* API to allow other modules to create Mandrill email patterns and site
  endpoints.
* Administrative functions to create / delete Mandrill Inbound Domains (e.g.
  reply.mysite.com).
* Administrative functions to disable or assign module implemented routes to a
  Mandrill Inbound Domain.
* A testing mode for developers / admins to log or email forward the inbound
  messages.

Similar Modules
---------------

[Mandrill Inbound](https://www.drupal.org/project/mandrill_inbound)
Mandrill Inbound provides similar functionality, however it is heavily based on
the Rules module. Not all sites utilize the Rules module and we didn't want to
require sites to install Rules simply to process email responses. It also
requires the older Mandrill 7.x-1.x module and has not been updated yet to
support Mandrill 7.x-2.x.

[Mandrill Incoming](https://www.drupal.org/project/mandrill_incoming)
Mandrill Incoming provides similar functionality, however it requires the use
of the Service module. Not all sites utilize the Services module and we didn't
want to require sites to install Services simply to receive an inbound POST
from Mandrill. It also appears that this module is still in the development
phase.

[Mailhandler](https://www.drupal.org/project/mailhandler)
Mailhandler has been the go-to module in the past for taking incoming emails
and turning those into nodes, comments, etc. It utilizes the Feeds module to
read email from an inbox and process those. If your site is not using Mandrill
then this is the most likely candidate for processing incoming emails.

Instructions
------------

**Requirements**
* A Mailchimp/Mandrill account.
* [Mandrill](https://www.drupal.org/project/mandrill) 7.x-2.x (or higher)
* Mandrill must be able to send a POST to the server.

**Installation / Configuration**
1. Install the Mandrill and Mandrill Inbound API modules and their requirements.
2. Create a Mandrill Inbound Domain.
3. Add the Mandrill provided MX records to the site's DNS record.
4. Install a module that depends on Mandrill Inbound API.
5. Assign the new module's "Route" to a Mandrill Inbound Domain.
6. Drink a beer.

**Testing**
If the modules are installed on a site running locally, it is likely that
Mandrill will not be able to POST to your local server. I'd recommend using
the free service ngrok.

* [ngrok](https://ngrok.com/) - Secure Tunnels to Localhost
  Download ngrok to your local server and follow the instructions from the
  website. It can be as simply as running the following to allow ngrok to
  securely post to port 80 on your local server.
  $ ./ngrok http 80

@todo -- need testing steps with screenshots.

Module Developers
-----------------
@todo

Maintainers
-----------
Craig Aschbrenner (https://drupal.org/u/caschbre) (http://foggyperspective.com)
